#!/bin/bash
if [ -n "$CODER" ]; then
    DOTFILE_PATH=$HOME/.config/coderv2/dotfiles
else
    DOTFILE_PATH=$HOME/.dotfiles
fi

ln -s $DOTFILE_PATH/.bash_aliases $HOME/.bash_aliases
